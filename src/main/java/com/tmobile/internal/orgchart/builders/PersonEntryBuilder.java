package com.tmobile.internal.orgchart.builders;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.springframework.stereotype.Component;

import com.tmobile.internal.orgchart.objects.EmploymentStatus;
import com.tmobile.internal.orgchart.objects.PersonEntry;

@Component
public class PersonEntryBuilder {	
	/**
	 * Builds a PersonEntry with a depth of 1, meaning we won't traverse the tree.
	 * 
	 * @implNote Once we move from ADAM to MADAM, most of this code will hopefully need to be refactored.
	 * 
	 * @return PersonEntry
	 */
	public PersonEntry buildPersonEntry(Attributes attrs) throws NamingException {
		PersonEntry person = new PersonEntry();

		person.setFirstName(attrs.get("givenName").get().toString());
		person.setLastName(attrs.get("sn").get().toString());
		
		Attribute cnAttribute = attrs.get("cn");
		person.setFullName(cnAttribute == null ? String.format("%s %s", person.getFirstName(), person.getLastName()) : cnAttribute.get().toString());

		Attribute nicknameAttribute = attrs.get("fonNickname");
		person.setNickName(nicknameAttribute == null ? null
				: nicknameAttribute.get().toString().equals(person.getFirstName()) ? null : nicknameAttribute.get().toString());
		
		person.setTitle(attrs.get("title").get().toString());
		person.setCorporateId(attrs.get("fonCorporateID").get().toString());
		
		Attribute companyAttribute = attrs.get("company");
		person.setCompany(companyAttribute == null ? null : companyAttribute.get().toString());
		
		person.setAdId(attrs.get("fonEnterpriseID").get().toString());
		person.setManagerCid(attrs.get("fonManagerCID").get().toString());
		
		Attribute departmentAttribute = attrs.get("department");
		person.setDepartment(departmentAttribute == null ? null : departmentAttribute.get().toString());
		
		Attribute managerAttribute = attrs.get("manager");
		person.setManagerDn(managerAttribute == null ? String.format("fonCorporateID=%s,OU=People,O=Spring,C=US", person.getManagerCid()) : attrs.get("manager").get().toString());
		Attribute managerNameAttribute = attrs.get("nxtmanagername");
		person.setManagerName(managerNameAttribute == null ? null : managerNameAttribute.get().toString());
		
		Attribute directReportsAttribute = attrs.get("fonDirectRpts");
		person.setHasDirectReports(directReportsAttribute == null ? false : directReportsAttribute.get().toString().equals("Y"));
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyy");
		
		Attribute startDateAttribute = attrs.get("fonStartDate");
		if (startDateAttribute != null) {
			String startDateOriginalFormat = startDateAttribute.get().toString();
			String startDateDay = startDateOriginalFormat.substring(startDateOriginalFormat.length() - 2);
			String startDateMonth = startDateOriginalFormat.substring(startDateOriginalFormat.length() - 4, startDateOriginalFormat.length() - 2);
			String startDateYear = startDateOriginalFormat.substring(0, startDateOriginalFormat.length() - 4); // gotta account for the year 10000 AD lmao
			try {
				person.setStartDate(dateFormatter.parse(String.format("%s/%s/%s", startDateMonth, startDateDay, startDateYear)));
			} catch (ParseException e) {
				person.setStartDate(null);
			}
		} else {
			person.setStartDate(null);
		}
		
		Attribute endDateAttribute = attrs.get("fonEndDate");
		// If the person is an employee, then fonEndDate won't exist
		if (endDateAttribute != null) {
			String endDateOriginalFormat = endDateAttribute.get().toString();
			String endDateDay = endDateOriginalFormat.substring(endDateOriginalFormat.length() - 2);
			String endDateMonth = endDateOriginalFormat.substring(endDateOriginalFormat.length() - 4, endDateOriginalFormat.length() - 2);
			String endtDateYear = endDateOriginalFormat.substring(0, endDateOriginalFormat.length() - 4); // gotta account for the year 10000 AD lmao
			try {
				person.setEndDate(dateFormatter.parse(String.format("%s/%s/%s", endDateMonth, endDateDay, endtDateYear)));
			} catch (ParseException e) {
				person.setEndDate(null);
			}
		} else {
			person.setEndDate(null);
		}
		
		Attribute accountNameAttribute = attrs.get("sAMAccountName");
		person.setAccountName(accountNameAttribute == null ? null : accountNameAttribute.get().toString());
		
		Attribute nonEmployeeTypeAttribute = attrs.get("fonNonEmployeeType");
		person.setNonEmployeeType(nonEmployeeTypeAttribute == null ? null : nonEmployeeTypeAttribute.get().toString());
		
		Attribute emailAttribute = attrs.get("mail");
		person.setEmail(emailAttribute == null ? null : emailAttribute.get().toString());
		
		Attribute employmentStatusAttribute = attrs.get("fonEmpStatus");
		if (employmentStatusAttribute == null) {
			person.setEmploymentStatus(EmploymentStatus.UNKNOWN); // this makes me scratch my head
		} else {
			char employmentStatusChar = employmentStatusAttribute.get().toString().charAt(0); // the status is either (A, P, S, or T)
			person.setEmploymentStatus(EmploymentStatus.get(employmentStatusChar));
		}
		
		person.setManager(null);
		
		person.setDisplayName(attrs.get("displayName").get().toString());
		
		Attribute mobileAttribute = attrs.get("mobile");
		person.setMobileNumber(mobileAttribute == null ? null : mobileAttribute.get().toString());
		
		// Now to determine if a user is a contractor.
		person.setIsContractor(attrs.get("employeeType").get().toString().equals("N"));

		return person;
	}
}
