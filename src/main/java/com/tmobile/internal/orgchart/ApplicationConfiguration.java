//package com.tmobile.internal.orgchart;
//
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.ViewResolver;
//import org.springframework.web.servlet.config.annotation.CorsRegistry;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//import org.springframework.web.servlet.view.InternalResourceViewResolver;
//
//@Configuration
//@EnableWebMvc
//public class ApplicationConfiguration extends WebMvcConfigurerAdapter {
//
//    @Bean
//    public ViewResolver jspViewResolver() {
//        InternalResourceViewResolver bean = new InternalResourceViewResolver();
//        bean.setPrefix("/WEB-INF/classes/templates/");
//        bean.setSuffix(".jsp");
//        return bean;
//    }
//    
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        try {
//			registry.addMapping("/**").allowedOrigins("http://localhost:3000", String.format("http://%s:3000", InetAddress.getLocalHost().getHostAddress()));
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }
//
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        try {
//			
//			registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }
//}