package com.tmobile.internal.orgchart.services;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tmobile.internal.orgchart.builders.PersonEntryBuilder;
import com.tmobile.internal.orgchart.objects.PeopleSearchResult;
import com.tmobile.internal.orgchart.objects.PersonEntry;

import javax.naming.*;
import javax.naming.directory.*;

@Component
public class LDAPService {
	// TODO: Move these to a more secure location. I'm just testing things out here.
	private static final String user = "CN=USAM-Users,OU=ServiceIDs,O=Sprint,C=US";
	private static final String pass = "Eight7TwoSeven";

	private static final String host = "IDMData.corp.sprint.com";
	private static final int port = 389;

	Logger logger = LoggerFactory.getLogger(LDAPService.class);

	DirContext context;

	@Autowired
	PersonEntryBuilder personBuilder;

	public static final String escapeLDAPSearchFilter(String filter) {
		StringBuffer sb = new StringBuffer(); // If using JDK >= 1.5 consider using StringBuilder
		for (int i = 0; i < filter.length(); i++) {
			char curChar = filter.charAt(i);
			switch (curChar) {
			case '\\':
				sb.append("\\5c");
				break;
			case '*':
				sb.append("\\2a");
				break;
			case '(':
				sb.append("\\28");
				break;
			case ')':
				sb.append("\\29");
				break;
			case '\u0000':
				sb.append("\\00");
				break;
			default:
				sb.append(curChar);
			}
		}
		return sb.toString();
	}

	public void createConnection() {
		logger.info("Connecting to LDAP...");

		try {
			NamingEnumeration<?> namingEnum = context.search("O=Sprint,C=US", "(objectClass=*)",
					getSimpleSearchControls());
			if (namingEnum.hasMore()) {
				logger.info("Already connected to LDAP.");
				return;
			}
		} catch (NamingException exception) {
			// eat the exception because it's what we expected.
		} catch (NullPointerException exception) {
			// eat this one too
		}

		Hashtable<String, String> environment = new Hashtable<String, String>();

		environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		environment.put(Context.PROVIDER_URL, String.format("ldap://%s:%s", host, port));
		environment.put(Context.SECURITY_AUTHENTICATION, "simple");
		environment.put(Context.SECURITY_PRINCIPAL, user);
		environment.put(Context.SECURITY_CREDENTIALS, pass);

		try {
			context = new InitialDirContext(environment);
			logger.info("Connected to LDAP!");
		} catch (AuthenticationNotSupportedException exception) {
			logger.error("The authentication method is not supported by the server");
		} catch (AuthenticationException exception) {
			logger.error("Incorrect password or username");
		} catch (NamingException exception) {
			logger.error("Error when trying to create the context");
		}
	}

	public void closeConnection() {
		logger.info("Disconnecting from LDAP...");

		try {
			context.close();
		} catch (NamingException exception) {
			logger.error("Error when trying to close the context");
		}
	}

	public PersonEntry lookupByCID(String cid, boolean recursive) {
		createConnection();

		cid = escapeLDAPSearchFilter(cid);

		PersonEntry retval = null;
		String lookup = String.format("(&(fonCorporateID=%s)(!(fonEmpStatus=T)))", cid);

		try {
			NamingEnumeration<?> namingEnum = context.search("OU=People,O=Sprint,C=US", lookup,
					getSimpleSearchControls());

			// there can only be one result since corporate ids are unique, so we return the
			// first one.
			while (namingEnum.hasMore()) {
				retval = new PersonEntry();
				SearchResult result = (SearchResult) namingEnum.next();
				Attributes attrs = result.getAttributes();
				retval = personBuilder.buildPersonEntry(attrs);

				if (recursive) {
					if (retval.getManagerCid().equals(retval.getCorporateId())) {
						break;
					}

					PersonEntry managerEntry = lookupByCID(retval.getManagerCid(), true);

					retval.setManager(managerEntry);

					// Weird edge case where some people don't have a manager name.
					if (retval.getManagerName() == null) {
						retval.setManagerName(managerEntry.getFullName());
					}
				}

				break;
			}

			namingEnum.close();
		} catch (NamingException exception) {
			logger.error("There was an exception while searching by cid: " + exception.getMessage());
			logger.error(exception.getStackTrace().toString());
		}

		closeConnection();

		return retval;
	}

	public PeopleSearchResult lookupByName(String name) {
		createConnection();

		name = escapeLDAPSearchFilter(name);

		PeopleSearchResult resultObj = new PeopleSearchResult();
		List<PersonEntry> peopleList = new ArrayList<PersonEntry>();

		String[] splitName = name.split("\\s+");

		String displayNameLookup = "";

		if (splitName.length == 2) {
			displayNameLookup = String.format("(displayName=%s, %s*)(displayName=%s, %s*)", splitName[1], splitName[0], splitName[0], splitName[1]);
		} else if (splitName.length == 3) {
			// Just in case they look up with a middle initial or name or they have a split
			// last name (Van Fleet, etc)
			if (splitName[1].length() == 1) {
				displayNameLookup = String.format("(displayName=%s*, %s%s)(displayName=%s, %s*%s)", splitName[2], splitName[0],
						" " + splitName[1], splitName[0], splitName[2], " " + splitName[1]);
			} else {
				displayNameLookup = String.format("(displayName=%s %s, %s*)(displayName=%s, %s%s)", splitName[1], splitName[2], splitName[0], splitName[0], splitName[1], " " + splitName[2] );
			}
			/*
			 * This is good enough for the time being. Some edge cases that will break
			 * things: * if the first name is separated with a space. * searching with first
			 * name, middle initial, and separated last name
			 */
		}
		

		// There is a bug with this that searching by "Last Name Nick Name" doesn't return a result
		String additionalSearchQuery = "";
		if (splitName.length >= 2) {
			additionalSearchQuery = String.format(
					"(|(&(givenname=%s*)(sn=%s*))(&(givenname=%s*)(sn=%s*)))",
					splitName[1], splitName[0], splitName[0], splitName[1]);
		}

		String lookup = String.format(
				"(&(|(cn=%s)(givenName=%s)(sn=%s)(fonNickname=%s)%s(displayName=%s*)(displayName=*, %s*)%s)(!(fonEmpStatus=T)))", name,
				name, name, name, displayNameLookup, name, name, additionalSearchQuery);

		try {
			NamingEnumeration<?> namingEnum = context.search("OU=People,O=Sprint,C=US", lookup,
					getSimpleSearchControls());

			while (namingEnum.hasMore()) {
				SearchResult result = (SearchResult) namingEnum.next();
				Attributes attrs = result.getAttributes();

				peopleList.add(personBuilder.buildPersonEntry(attrs));
			}

			resultObj.setResults(peopleList);
		} catch (NamingException exception) {
			logger.error("There was an exception while searching by name: " + exception.getMessage());
			logger.error(exception.getStackTrace().toString());
		}

		closeConnection();

		return resultObj;
	}

	public PersonEntry lookupByEmail(String email) {
		createConnection();

		email = escapeLDAPSearchFilter(email);

		PersonEntry resultObj = null;

		String lookup = String.format("(&(mail=%s)(!(fonEmpStatus=T)))", email);

		try {
			NamingEnumeration<?> namingEnum = context.search("OU=People,O=Sprint,C=US", lookup,
					getSimpleSearchControls());

			// there can only be one result since email addresses are unique, so we return
			// the first one.
			while (namingEnum.hasMore()) {
				resultObj = new PersonEntry();
				SearchResult result = (SearchResult) namingEnum.next();
				Attributes attrs = result.getAttributes();
				resultObj = personBuilder.buildPersonEntry(attrs);

				break;
			}

			namingEnum.close();
		} catch (NamingException exception) {
			logger.error("There was an exception while searching by email: " + exception.getMessage());
			logger.error(exception.getStackTrace().toString());
		}

		closeConnection();

		return resultObj;
	}

	public PeopleSearchResult getDirectReportsForManager(String managerCid) {
		createConnection();

		managerCid = escapeLDAPSearchFilter(managerCid);

		PeopleSearchResult retval = new PeopleSearchResult();
		List<PersonEntry> peopleList = new ArrayList<PersonEntry>();

		String lookup = String.format("(&(fonManagerCID=%s)(!(fonEmpStatus=T)))", managerCid);

		try {
			NamingEnumeration<?> namingEnum = context.search("OU=People,O=Sprint,C=US", lookup,
					getSimpleSearchControls());

			while (namingEnum.hasMore()) {
				SearchResult result = (SearchResult) namingEnum.next();
				Attributes attrs = result.getAttributes();

				peopleList.add(personBuilder.buildPersonEntry(attrs));
			}

			retval.setResults(peopleList);

			namingEnum.close();
		} catch (NamingException exception) {
			logger.error("There was an exception while searching by fonReportsToCID: " + exception.getMessage());
			logger.error(exception.getStackTrace().toString());
		}

		closeConnection();

		return retval;
	}

	public PersonEntry lookupByNtId(String ntid) {
		PersonEntry resultObj = null;

		ntid = escapeLDAPSearchFilter(ntid);

		createConnection();

		String lookup = String.format("(&(sAMAccountName=%s)(!(fonEmpStatus=T)))", ntid);

		try {
			NamingEnumeration<?> namingEnum = context.search("OU=People,O=Sprint,C=US", lookup,
					getSimpleSearchControls());

			// there can only be one result since ntid's are unique, so we return the first
			// one.
			while (namingEnum.hasMore()) {
				resultObj = new PersonEntry();
				SearchResult result = (SearchResult) namingEnum.next();
				Attributes attrs = result.getAttributes();
				resultObj = personBuilder.buildPersonEntry(attrs);

				break;
			}

			namingEnum.close();
		} catch (NamingException exception) {
			logger.error("There was an exception while searching by AD/NT ID: " + exception.getMessage());
			logger.error(exception.getStackTrace().toString());
		}

		closeConnection();

		return resultObj;
	}

	private SearchControls getSimpleSearchControls() {
		SearchControls searchControls = new SearchControls();

		searchControls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
		searchControls.setTimeLimit(10000);

		return searchControls;
	}
}
