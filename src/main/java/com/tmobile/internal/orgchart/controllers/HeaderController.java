package com.tmobile.internal.orgchart.controllers;

import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HeaderController {
	 @RequestMapping("/headers")
	    public ResponseEntity index(HttpServletResponse response) {
		 
		 Collection<String> headerNames = response.getHeaderNames();
		 Map map=new HashMap<String,String>();
		 
		 Iterator<String> it=headerNames.iterator();
		 while(it.hasNext()) {
		   String headerName = (String)it.next();
		   map.put(headerName,response.getHeader(headerName));
		  
		 }
	        return ResponseEntity.ok(map);
	    }
}



