package com.tmobile.internal.orgchart.controllers;

import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tmobile.internal.orgchart.objects.PeopleSearchResult;
import com.tmobile.internal.orgchart.objects.PersonEntry;
import com.tmobile.internal.orgchart.services.LDAPService;

@RestController
@RequestMapping("/search")
public class SearchController {
	@Autowired
	LDAPService ldapService;
	
	
	@GetMapping("/by-cid")
	public ResponseEntity<PersonEntry> searchByCID(HttpServletResponse response, @RequestParam String cid, @RequestParam Optional<Boolean> recursive,@RequestHeader Map<String,String> headers) {
		response.addHeader("Cache-Control", CacheControl.noCache().getHeaderValue());
		headers.forEach((key, value) -> {
			System.out.println("key: " + key + " , value: " + value );
			});
		PersonEntry result = ldapService.lookupByCID(cid, recursive.orElse(false));
		
		if (result == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@CrossOrigin(origins={"http://labs.example.com:8080","https://labs.example.com:3000"})
	@GetMapping("/by-name")
	public ResponseEntity<PeopleSearchResult> searchByName(HttpServletResponse response, @RequestParam String name) {
		response.addHeader("Cache-Control", CacheControl.noCache().getHeaderValue());
		
		PeopleSearchResult result = ldapService.lookupByName(name);
		
		if (result.getResults() == null) {
			// maybe bad data from ADAM?
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		} else if (result.getResults().isEmpty()) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/by-email")
	public ResponseEntity<PersonEntry> searchByEmail(HttpServletResponse response, @RequestParam String email) {
		response.addHeader("Cache-Control", CacheControl.noCache().getHeaderValue());
		
		PersonEntry result = ldapService.lookupByEmail(email);
		
		if (result == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/by-manager")
	public ResponseEntity<PeopleSearchResult> searchByManager(HttpServletResponse response, @RequestParam String cid) {
		response.addHeader("Cache-Control", CacheControl.noCache().getHeaderValue());
		
		PeopleSearchResult result = ldapService.getDirectReportsForManager(cid);
		
		if (result == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);	
		}
		
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/by-ntid")
	public ResponseEntity<PersonEntry> searchByNtId(HttpServletResponse response, @RequestParam String ntid) {
		response.addHeader("Cache-Control", CacheControl.noCache().getHeaderValue());
		
		PersonEntry result = ldapService.lookupByNtId(ntid);
		System.out.println(ntid);
		
		if (result == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
}
