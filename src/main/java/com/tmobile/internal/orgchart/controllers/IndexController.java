package com.tmobile.internal.orgchart.controllers;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.CacheControl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexController {
	@GetMapping("/")
	public String index(HttpServletResponse response) {
		response.addHeader("Cache-Control", CacheControl.noCache().getHeaderValue());
		
		return "OK";
	}
	
	@GetMapping("keepalive.html")
	public String keepAlive(HttpServletResponse response) throws UnknownHostException {
		response.addHeader("Cache-Control", CacheControl.noCache().getHeaderValue());
		
		return String.format("Alive %s", InetAddress.getLocalHost().getHostName());
	}
}
