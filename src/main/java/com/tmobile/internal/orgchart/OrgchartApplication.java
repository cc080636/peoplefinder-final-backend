package com.tmobile.internal.orgchart;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class OrgchartApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrgchartApplication.class, args);
	}

}
