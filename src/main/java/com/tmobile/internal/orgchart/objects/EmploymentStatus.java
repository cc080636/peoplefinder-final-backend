package com.tmobile.internal.orgchart.objects;

import java.util.HashMap;
import java.util.Map;

public enum EmploymentStatus {
	ACTIVE('A'),
	LEAVE_WITH_PAY('P'),
	SUSPENDED('S'),
	TERMINATED('T'),
	UNKNOWN('Z'); // These codes map directly with AD except for Unknown. There are some discrepancies that having unknown make sense. 
	
	private char code;
	
	EmploymentStatus(char code) {
		this.code = code;
	}
	
	public char getCode() {
		return code;
	}
	
	// Lookup table
	private static final Map<Character, EmploymentStatus> lookup = new HashMap<>();
	
	static {
		for (EmploymentStatus es : EmploymentStatus.values()) {
			lookup.put(es.code, es);
		}
	}
	
	public static EmploymentStatus get(char code) {
		return lookup.get(code);
	}
}
