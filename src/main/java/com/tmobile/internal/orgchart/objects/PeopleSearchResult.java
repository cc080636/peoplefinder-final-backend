package com.tmobile.internal.orgchart.objects;

import java.util.List;

public class PeopleSearchResult {
	private List<PersonEntry> results;

	public List<PersonEntry> getResults() {
		return results;
	}

	public void setResults(List<PersonEntry> results) {
		this.results = results;
	}
}
