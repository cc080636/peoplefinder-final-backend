package com.tmobile.internal.orgchart.objects;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Some data out of a Person entry in LDAP.
 * 
 * @author Josh Kennedy
 */
public class PersonEntry {
	private String firstName;
	private String lastName;
	private String fullName;
	private String nickName;
	private String title;
	private String corporateId;
	private String company;
	private String adId;
	private String department;
	private PersonEntry manager;
	private String managerDn;
	private String managerName;
	private Boolean hasDirectReports;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date startDate;
	private String accountName;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date endDate;
	private String nonEmployeeType;
	private String email;
	private EmploymentStatus employmentStatus;
	private String managerCid;
	private String displayName;
	private String mobileNumber;
	private Boolean isContractor;

	// These aren't all of the attributes out of LDAP,
	// but these are the most important for our purpose.

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(String corporateId) {
		this.corporateId = corporateId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAdId() {
		return adId;
	}

	public void setAdId(String adId) {
		this.adId = adId;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public PersonEntry getManager() {
		return manager;
	}

	public void setManager(PersonEntry manager) {
		this.manager = manager;
	}

	public String getManagerDn() {
		return managerDn;
	}

	public void setManagerDn(String managerDn) {
		this.managerDn = managerDn;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public Boolean getHasDirectReports() {
		return hasDirectReports;
	}

	public void setHasDirectReports(Boolean hasDirectReports) {
		this.hasDirectReports = hasDirectReports;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getNonEmployeeType() {
		return nonEmployeeType;
	}

	public void setNonEmployeeType(String nonEmployeeType) {
		this.nonEmployeeType = nonEmployeeType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public EmploymentStatus getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(EmploymentStatus employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public String getManagerCid() {
		return managerCid;
	}

	public void setManagerCid(String managerCid) {
		this.managerCid = managerCid;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Boolean getIsContractor() {
		return isContractor;
	}

	public void setIsContractor(Boolean isContractor) {
		this.isContractor = isContractor;
	}
}
