<%@ page
        import="java.util.*"
                 contentType="text/html; charset=UTF-8"
%>
<%
        response.setHeader("Cache-Control","no-cache");
%>
<meta http-equiv="refresh" content="115">
<SCRIPT ID=clientEventHandlersJS LANGUAGE=javascript>
 <!--
 function showHide(divObj)
 {
        if (divObj.style.display == "none") {
                divObj.style.display = "block";
        } else  {
                divObj.style.display = "none";
        }
 }
 //-->
 </SCRIPT>

<!-- Include the cascading style sheet with the PepTalk definitions :  -->
<link rel="stylesheet" TYPE="text/css" HREF="Quk_PepTalk.css">

 <!-- Display the general request information for debugging purposes:  -->
 <br />
 <div width="100%" class="PepTalkHdrBlue" style="text-align:left; padding:3px;">

 <a name="reqiAnchor" href="#reqiAnchor"
    onclick="showHide(reqInfo);"
    class="PepTalkHdrBlue"
    style="text-decoration: underline; text-align: left; color: #FFFFFF;"
    alt="Click to Show/Hide">
        General Request Information
 </a>

 <div id="reqInfo" style="width:100%; display:block; float:none; ">
 <table width="100%">
 <tr><td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px; "><br />Name</td>
     <td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px; "><br />Value</td></tr>
 <%
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Servlet Spec Version Implemented</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + getServletConfig().getServletContext().getMajorVersion() + "." + getServletConfig().getServletContext().getMinorVersion() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Requested URL</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + HttpUtils.getRequestURL(request) + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Request Method</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getMethod() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Request URI</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getRequestURI() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Request Protocol</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getProtocol() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Servlet Path</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getServletPath() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Path Info</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getPathInfo() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Path Translated</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getPathTranslated() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Query String</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getQueryString() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Content Length</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getContentLength() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Content Type</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getContentType() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Server Name</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getServerName() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Server Port</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getServerPort() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Remote User</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getRemoteUser() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Remote Address</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getRemoteAddr() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Remote Host</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getRemoteHost() + "</td></tr>");
   out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px'> Authorization Scheme</td>" +
               "<td style='font-family:courier new; font-size:x-small; padding:3px'>" + request.getAuthType() + "</td></tr>");
 %>
 </table>
 </div>
 </div>


 <%
 Enumeration enumVar = null;
 %>


 <!-- Display the request headers for debugging purposes:  -->
 <br />
 <div width="100%" class="PepTalkHdrBlue" style="text-align:left; padding:3px;">

 <a name="reqhAnchor" href="#reqhAnchor"
    onclick="showHide(reqHeaders);"
    class="PepTalkHdrBlue"
    style="text-decoration: underline; text-align: left; color: #FFFFFF;"
    alt="Click to Show/Hide">
        Request Headers
 </a>

 <div id="reqHeaders" style="width:100%; display:block; float:none; ">
 <table width="100%">
 <tr><td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px; "><br />Name</td>
     <td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px; "><br />Value</td></tr>
 <%
 enumVar = request.getHeaderNames();
 while (enumVar.hasMoreElements()) {
     String name = (String)enumVar.nextElement();
     out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px;'>" +
                 name + "</td><td style='font-family:courier new; font-size:x-small; padding:3px;'>" +
                 request.getHeader(name) + "</td></tr>");
 }
 %>
 </table>
 </div>
 </div>


 <!-- Display the request parameters for debugging purposes:  -->
 <br />
 <div width="100%" class="PepTalkHdrBlue" style="text-align:left; padding:3px;">

 <a name="reqpAnchor" href="#reqpAnchor"
    onclick="showHide(reqParameters);"
    class="PepTalkHdrBlue"
    style="text-decoration: underline; text-align: left; color: #FFFFFF;"
    alt="Click to Show/Hide">
        Request Parameters
 </a>

 <div id="reqParameters" style="width:100%; display:block; float:none; ">
 <table width="100%">
 <tr><td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px; "><br />Name</td>
     <td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px; "><br />Value</td></tr>
 <%
 enumVar = request.getParameterNames();
 while(enumVar.hasMoreElements()){
    String key = (String)enumVar.nextElement();
    String[] paramValues = request.getParameterValues(key);
    for(int i=0;i < paramValues.length;i++){
        out.println("<tr style='background-color: #FFFFFF;'><td style='font-family:courier new; font-size:x-small; padding:3px;'>" +
                    key + "</td><td style='font-family:courier new; font-size:x-small; padding:3px;'>" +
                    paramValues[i] + "</td></tr>");
    }
 }
 %>
 </table>
 </div>
 </div>


 <!-- Display the request attributes for debugging purposes:  -->
 <br />
 <div width="100%" class="PepTalkHdrBlue" style="text-align:left; padding:3px;">

 <a name="reqaAnchor" href="#reqaAnchor"
    onclick="showHide(reqAttributes);"
    class="PepTalkHdrBlue"
    style="text-decoration: underline; text-align: left; color: #FFFFFF;"
    alt="Click to Show/Hide">
        Request Attributes
 </a>

 <div id="reqAttributes" style="width:100%; display:block; float:none; ">
 <table width="100%">
 <tr><td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px;"><br />Name</td>
     <td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px;"><br />Value</td></tr>
 <%
 enumVar = request.getAttributeNames();
 while (enumVar.hasMoreElements()) {
     String name = (String)enumVar.nextElement();
     out.println("<tr style='background-color: #FFFFFF;'><td style='font-family: courier new; font-size: x-small; padding:3px;'>" +
                 name + "</td><td style='font-family: courier new; font-size: x-small; padding:3px;'>" +
                 request.getAttribute(name) + "</td></tr>");
 }
 %>
 </table>
 </div>
 </div>

 <!-- Display the session attributes for debugging purposes:  -->
 <br />
 <div width="100%" class="PepTalkHdrBlue" style="text-align:left; padding:3px;">

 <a name="sessAnchor" href="#sessAnchor"
    onclick="showHide(sessAttributes, sessAnchor);"
    class="PepTalkHdrBlue"
    style="text-decoration: underline; text-align: left; color: #FFFFFF;"
    alt="Click to Show/Hide">
        Session Attributes
 </a>

 <div id="sessAttributes" style="width:100%; display:block; float:none; ">
 <table width="100%">
 <tr><td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px "><br />Name</td>
     <td class="PepTalkHdrBlue" style="font-size: small; vertical-align: bottom; padding:3px; "><br />Value</td></tr>
 <%
 enumVar = session.getAttributeNames();
 while (enumVar.hasMoreElements()) {
     String name = (String)enumVar.nextElement();
     out.println("<tr style='background-color: #FFFFFF;'><td style='font-family: courier new; font-size: x-small; padding:3px;'>" +
                 name + "</td><td style='font-family: courier new; font-size: x-small; padding:3px;'>" +
                 session.getAttribute(name) + "</td></tr>");
 }
 %>
 </table>
 </div>
 </div>