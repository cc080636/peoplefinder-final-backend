var faker = require("faker");
var appender =require('file-appender');
var appRouter = function (app) {
var ou=require('fs').createWriteStream('./log.txt')
const log4js = require('log4js'); // include log4js
log4js.configure({
    appenders: { app: { type: 'file', filename: 'app.log' } },
    categories: { default: { appenders: ['app'], level: 'debug' } }
  });


  app.post("/", function (req, res) {
      const logger = log4js.getLogger('app');
      console.log(req.body);
      logger.debug(req.body);
    res.status(200).send({ message: 'Welcome to our restful API' });
  });

}

module.exports=appRouter;